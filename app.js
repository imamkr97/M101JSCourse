var http = require('http');

var server = http.createServer(function (req, res) {
	res.writeHead(200, {"Content-Type": "text/plain"});
	res.end("Hello, World\n");
	console.log("Request made by " + req.url);	
});

server.listen(8000); //PORT
console.log("Server running at http://localhost:8000");
